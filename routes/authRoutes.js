const { Router } = require('express');
const AuthService = require('../services/authService');
const { loginValidation } = require('../middlewares/login.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post(
  '/login',
  loginValidation,
  (req, res, next) => {
    try {
      const { email, password } = req.body;
      const user = AuthService.login((user) => user.email === email && user.password === password);
      if (!user) {
        const newError = new Error('Authentication error');
        newError.statusCode = 401;
        throw newError;
      }
      res.payload = user;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
