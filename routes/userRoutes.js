const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get(
  '/',
  (req, res, next) => {
    res.payload = UserService.getAll();
    next();
  },
  responseMiddleware
);

router.get(
  '/:id',
  (req, res, next) => {
    const { id: userId } = req.params;
    const user = UserService.search((user) => user.id === userId);
    if (user) {
      res.payload = user; 
    } else {
      const err = new Error("User not found");
      err.statusCode = 404;
      res.err = err;
    }

    next();
  },
  responseMiddleware
);

router.post(
  '/',
  createUserValid,
  (req, res, next) => {
    if (res.userData) {
      res.payload = UserService.create(res.userData);
    }
    next();
  },
  responseMiddleware
);

router.put(
  '/:id',
  updateUserValid,
  (req, res, next) => {
    if (res.userData) {
      const { id: userId } = req.params;
      res.payload = UserService.update(userId, res.userData);
    }
    next();
  },
  responseMiddleware
);

router.delete(
  '/:id',
  (req, res, next) => {
    const { id: userId } = req.params;
    const user = UserService.delete(userId);
    if(user) {
      res.payload = user; 
    } else {
      const err = new Error("User not found");
      err.statusCode = 404;
      res.err = err;
    }

    next();
  },
  responseMiddleware
);

module.exports = router;
