const { Router } = require('express');
const FightService = require('../services/fightService');
const {
  createFightValid,
  updateFightValid,
} = require('../middlewares/fight.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get(
  '/',
  (req, res, next) => {
    res.payload = FightService.getAll();
    next();
  },
  responseMiddleware
);

router.get(
  '/:id',
  (req, res, next) => {
    const { id } = req.params;
    const fight = FightService.search((fight) => fight.id === id);
    if (fight) {
      res.payload = fight;
    } else {
      const err = new Error('Fight not found');
      err.statusCode = 404;
      res.err = err;
    }
    next();
  },
  responseMiddleware
);

router.post(
  '/',
  createFightValid,
  (req, res, next) => {
    if (res.userData) {
      res.payload = FightService.create(res.userData);
    }
    next();
  },
  responseMiddleware
);

// update only log
router.put(
  '/:id',
  updateFightValid,
  (req, res, next) => {
    if (res.userData) {
      res.payload = FightService.update(req.params.id, res.userData);
    }
    next();
  },
  responseMiddleware
);

router.delete(
  '/:id',
  (req, res, next) => {
    const { id } = req.params;
    const fight = FightService.delete(id);
    if (fight) {
      res.payload = fight;
    } else {
      const err = new Error('Fight not found');
      err.statusCode = 404;
      res.err = err;
    }
    next();
  },
  responseMiddleware
);

module.exports = router;
