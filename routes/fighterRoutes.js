const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const {
  createFighterValid,
  updateFighterValid,
} = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get(
  '/',
  (req, res, next) => {
    res.payload = FighterService.getAll();
    next();
  },
  responseMiddleware
);

router.get(
  '/:id',
  (req, res, next) => {
    const {id: fighterId} = req.params;
    const fighter = FighterService.search(fighter => fighter.id === fighterId);
    if (fighter) {
      res.payload = fighter
    } else {
      const err = new Error("Fighter not found");
      err.statusCode = 404;
      res.err = err;
    }
    next();
  },
  responseMiddleware
);

router.post(
  '/',
  createFighterValid,
  (req, res, next) => {
    if (res.userData) {
      try {
        res.payload = FighterService.create(res.userData);
      } catch (err) {
        res.err = err;
      }
    }
    next();
  },
  responseMiddleware
);

router.put(
  '/:id',
  updateFighterValid,
  (req, res, next) => {
    if (res.userData) {
      const { id: fighterId } = req.params;
      res.payload = FighterService.update(fighterId, res.userData);
    }
    next();
  },
  responseMiddleware
);

router.delete(
  '/:id',
  (req, res, next) => {
    const {id: fighterId} = req.params;
    const fighter = FighterService.delete(fighterId);
    if (fighter) {
      res.payload = fighter;
    } else{
      const err = new Error("Fighter not found");
      err.statusCode = 404;
      res.err = err;
    }
    next();
  },
  responseMiddleware
);

module.exports = router;
