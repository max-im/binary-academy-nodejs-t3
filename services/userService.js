const { UserRepository } = require('../repositories/userRepository');
class UserService {
  create(userData) {
    return UserRepository.create(userData);
  }
  
  getAll() {
    return UserRepository.getAll();
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  update(id, dataToUpdate) {
    return UserRepository.update(id, dataToUpdate);
  }
  
  delete(userId) {
    const userArr = UserRepository.delete(userId);
    if (userArr.length) {
      return userArr[0];
    } 
    return null;
  }
}

module.exports = new UserService();
