const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  getAll() {
    return FighterRepository.getAll();
  }

  search(fighterId) {
    return FighterRepository.getOne(fighterId);
  }

  create(data) {
    return FighterRepository.create(data);
  }

  update(id, dataToUpdate) {
    return FighterRepository.update(id, dataToUpdate);
  }

  delete(fighterId) {
    const fighterArr = FighterRepository.delete(fighterId);
    if (fighterArr.length) {
      return fighterArr[0]
    }
    return null;
  }
}

module.exports = new FighterService();
