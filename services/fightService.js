const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
  getAll() {
    return FightRepository.getAll();
  }

  search(search) {
    const item = FightRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  create(data) {
    return FightRepository.create(data);
  }

  update(id, dataToUpdate) {
    return FightRepository.update(id, dataToUpdate);
  }

  delete(fightId) {
    const fightArr = FightRepository.delete(fightId);
    if (fightArr.length) {
      return fightArr[0];
    } 
    return null;
  }
}

module.exports = new FightersService();
