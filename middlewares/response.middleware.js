const responseMiddleware = (req, res, next) => {  
  if (res.err) {
    
    const errorMessage = handleError(res.err);
    const code = res.err.statusCode || 400;
    return res.status(code).json({ error: true, message: errorMessage });
  } else if (res.payload) {
    return res.json(res.payload);
  } else res.end();
};


function handleError(err) {
  const defaultErrorMessage = "Error occurred, try again later";
  if (!err) return defaultErrorMessage;
  let errorMessage = defaultErrorMessage;
  if(typeof err === 'string') {
    errorMessage = err;
  } else if(Array.isArray(err)) {
    errorMessage = err.join("; \n");
  } else if(err.message && typeof err.message === 'string') {
    errorMessage = err.message
  }
  return errorMessage 
}

exports.responseMiddleware = responseMiddleware;
