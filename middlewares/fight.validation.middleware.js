const { fight } = require('../models/fight');
const { modelValidate } = require('../models/validate.models');
const FighterService = require('../services/fighterService');
const FightService = require('../services/fightService');
const { bodyHasId } = require('./helpers');

const createFightValid = (req, res, next) => {
  const idErr = bodyHasId(req.body);
  if (idErr) {
    res.err = idErr;
    return next();
  }

  const fightValidateResult = modelValidate(fight, req.body);
  if (fightValidateResult.error) {
    res.err = fightValidateResult.payload;
  } else {
    const firstFighterExist = FighterService.search(
      (fighter) => fighter.id === fightValidateResult.payload.fighter1
    );
    const secondFighterExist = FighterService.search(
      (fighter) => fighter.id === fightValidateResult.payload.fighter2
    );
    if (fightValidateResult.payload.fighter1 === fightValidateResult.payload.fighter2) {
      res.err = 'Cant put same fighters';
    } else if (!firstFighterExist) {
      const err = new Error('Fighter1 not found');
      err.statusCode = 404;
      res.err = err;
    } else if (!secondFighterExist) {
      const err = new Error('Fighter2 not found');
      err.statusCode = 404;
      res.err = err;
    } else {
      res.userData = { ...fightValidateResult.payload, log: [] };
    }
  }

  next();
};

const updateFightValid = (req, res, next) => {
  const idErr = bodyHasId(req.body);
  if (idErr) {
    res.err = idErr;
    return next();
  }

  if (Object.keys(req.body).length !== 1) {
    res.err = 'Invalid request';
    return next();
  }

  if (!('log' in req.body)) {
    res.err = 'log field is required';
    return next();
  }

  const fightId = FightService.search((fight) => fight.id === req.params.id);
  if (!fightId) {
    const err = new Error('Fight not found');
    err.statusCode = 404;
    res.err = err;
  } else {
    res.userData = { log: req.body.log };
  }

  next();
};

exports.createFightValid = createFightValid;
exports.updateFightValid = updateFightValid;
