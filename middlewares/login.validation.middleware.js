const validator = require("validator");

const loginValidation = (req, res, next) => {
    const {email, password} = req.body;
    const emailVal = email ? email.toString() : ""; 
    const passwordVal = password ? password.toString() : ""; 
    const errors = [];

    if (validator.isEmpty(emailVal)) {
        errors.push("Email field is empty");
    } else if (!validator.isEmail(emailVal)) {
        errors.push("You pass an invalid email");
    }

    if (validator.isEmpty(passwordVal)) {
        errors.push("Password field is empty");
    }
    
    if(errors.length) {
        res.err = errors;
    }
    else {
        res.userData = {email: emailVal, password: passwordVal};
    }

  next();
};



exports.loginValidation = loginValidation;
