const { fighter } = require('../models/fighter');
const { modelValidate, validateField } = require('../models/validate.models');
const { bodyHasId } = require('./helpers');
const FighterService = require('../services/fighterService');

const createFighterValid = (req, res, next) => {
  const idErr = bodyHasId(req.body);
  if (idErr) {
    res.err = idErr;
    return next();
  }

  const fighterValidateResult = modelValidate(fighter, req.body);
  if (fighterValidateResult.error) {
    res.err = fighterValidateResult.payload;
  } else {
    const sameNameFighter = FighterService.search(fighter => fighter.name === fighterValidateResult.payload.name);
    if (sameNameFighter) {
      res.err = "Fighter with the name already exists";
    } else {
      res.userData = fighterValidateResult.payload;
    }
  }
  next();
};

const updateFighterValid = (req, res, next) => {
  const idErr = bodyHasId(req.body);
  if (idErr) {
    res.err = idErr;
    return next();
  }
  
  const targetFields = {};
  const errors = [];

  if (!Object.keys(req.body).length) {
    res.err = 'Invalid request';
    return next();
  }

  for (const key in req.body) {
    if (key in fighter) {
      const fieldErrors = validateField(req.body, fighter, key);
      if (fieldErrors.length) errors.push(...fieldErrors);
      targetFields[key] = req.body[key];
    } else {
      errors.push(`Invalid field ${key}`);
    }
  }

  if (!Object.keys(targetFields).length) {
    res.err = 'Invalid request';
    return next();
  }

  if (errors.length) {
    res.err = errors;
  } else {
    const sameNameFighter = FighterService.search(fighter => fighter.name === targetFields.name);
    if (sameNameFighter) {
      res.err = "Fighter with the name already exists";
    } else {
      res.userData = targetFields;
    }
  }
  next();
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
