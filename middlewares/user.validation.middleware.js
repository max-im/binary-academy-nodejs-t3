const { user } = require('../models/user');
const { modelValidate, validateField } = require('../models/validate.models');
const UserService = require('../services/userService');
const { bodyHasId } = require('./helpers');

const createUserValid = (req, res, next) => {
  const idErr = bodyHasId(req.body);
  if (idErr) {
    res.err = idErr;
    return next();
  }

  const userValidateResult = modelValidate(user, req.body);
  if (userValidateResult.error) {
    res.err = userValidateResult.payload;
  } else {
    const sameEmailUser = UserService.search(
      (item) => item.email === userValidateResult.payload.email
    );
    if (sameEmailUser) {
      res.err = 'User with the email already registred';
    }
    const samePhoneUser = UserService.search(
      (item) => item.phoneNumber === userValidateResult.payload.phone
    );
    if (samePhoneUser) {
      res.err = 'User with the phone already registred';
    }
    if (!sameEmailUser && !samePhoneUser) {
      res.userData = userValidateResult.payload;
    }
  }

  next();
};

const updateUserValid = (req, res, next) => {
  const idErr = bodyHasId(req.body);
  if (idErr) {
    res.err = idErr;
    return next();
  }

  const targetFields = {};
  const errors = [];

  if (!Object.keys(req.body).length) {
    res.err = 'Invalid request';
    return next();
  }

  for (const key in req.body) {
    if (key in user) {
      const fieldErrors = validateField(req.body, user, key);
      if (fieldErrors.length) errors.push(...fieldErrors);
      targetFields[key] = req.body[key];
    } else {
      errors.push(`Invalid field ${key}`);
    }
  }

  if (!Object.keys(targetFields).length) {
    res.err = 'Invalid request';
    return next();
  }

  if (errors.length) {
    res.err = errors;
  } else {
    res.userData = targetFields;
  }

  next();
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
