exports.bodyHasId = (body) => {
  if ('id' in body || 'ID' in body || 'Id' in body || 'iD' in body) {
    const err = new Error("Invalid field name 'id'");
    return err;
  }
  return null;
};
