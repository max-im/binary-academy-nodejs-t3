import { get, post, deleteReq, put } from "../requestHelper";
const entity = 'users'

export const createUser = async (body) => {
    return await post(entity, body);
}

export const getUsers = async () => {
    return await get(entity);
}

export const getUser = async (id) => {
    return await get(entity, id)
}

export const updateUser = async (id, body) => {
    return await put(entity, id, body);
}

export const deleteUser = async (id) => {
    return await deleteReq(entity, id);
}