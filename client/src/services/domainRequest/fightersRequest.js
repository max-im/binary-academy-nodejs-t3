import { get, post, deleteReq, put } from "../requestHelper";

const entity = 'fighters';

export const getFighters = async () => {
    return await get(entity);
}

export const createFighter = async (body) => {
    return await post(entity, body);
}

export const updateFighter = async (id, body) => {
    return await put(entity, id, body)
}

export const getFighter = async (id) => {
    return await get(entity, id);
}

export const deleteFighter = async (id) => {
    return await deleteReq(entity, id);
}