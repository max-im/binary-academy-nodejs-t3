import React from 'react';
import { Link } from 'react-router-dom';

export default function fighterItem({ fighterData, deleteFighter }) {
  return (
    <li className="fighrerItem">
      <div>
        <p className="fighrerItem__text">Name: {fighterData.name}</p>
        <p className="fighrerItem__text">Power: {fighterData.power}</p>
        <p className="fighrerItem__text">Defense: {fighterData.defense}</p>
        <p className="fighrerItem__text">Health: {fighterData.health}</p>
        <p className="fighrerItem__text">Created: {fighterData.createdAt}</p>
        {fighterData.updatedAt && (
          <p className="fighrerItem__text">Updated: {fighterData.updatedAt}</p>
        )}
      </div>
      <div className="fighrerItem__control">
        <Link
          className="mainMenu__link mainMenu__link_active"
          to={'/update-fighter/' + fighterData.id}
        >
          Update
        </Link>
        <i
          className="fas fa-trash-alt mainMenu__link mainMenu__link_active"
          onClick={() => deleteFighter(fighterData.id)}
        ></i>
      </div>
    </li>
  );
}
