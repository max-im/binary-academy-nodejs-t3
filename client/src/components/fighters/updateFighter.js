import React, { Component } from 'react';
import { getFighter, updateFighter } from '../../services/domainRequest/fightersRequest';
import { withRouter } from 'react-router-dom';

class UpdateFighter extends Component {
  state = {
    updated: {},
  };

  componentDidMount() {
    getFighter(this.props.match.params.id).then((fighter) => this.initFighter(fighter));
  }

  initFighter = (fighter) => {
      console.log(fighter)
    this.setState({
      name: fighter.name,
      power: fighter.power,
      defense: fighter.defense
    });
  };

  onChange = (e) => {
    const { name, value } = e.target;

    const updated = { ...this.state.updated, [name]: value };
    this.setState({ [name]: value, updated });
  };

  onSubmit = (e) => {
    e.preventDefault();
    updateFighter(this.props.match.params.id, this.state.updated).then(() =>
      this.props.history.push('/fighters')
    );
  };

  render() {
    return (
      <div id="figh-wrapper">
        <div>
          <h3>Update Fighter</h3>
          <form onSubmit={this.onSubmit}>
            <input
              type="text"
              name="name"
              value={this.state.name}
              onChange={this.onChange}
              placeholder="Name"
            />
            <input
              type="number"
              name="power"
              value={this.state.power}
              onChange={this.onChange}
              placeholder="power"
            />
            <input
              type="number"
              name="defense"
              value={this.state.defense}
              onChange={this.onChange}
              placeholder="defense"
            />
            
            <button className="mainMenu__link mainMenu__link_active" type="submit">
              Save
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default withRouter(UpdateFighter);
