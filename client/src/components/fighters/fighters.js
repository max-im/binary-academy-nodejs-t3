import React, { Component } from 'react';
import { getFighters, deleteFighter } from '../../services/domainRequest/fightersRequest';
import FighterItem from './fighterItem';
import './fighters.css';

export default class Fighters extends Component {
  state = {
    fighters: [],
  };

  componentDidMount() {
      this.onGetFighters();
  }

  onGetFighters() {
    getFighters().then((fighters) => this.setState({ fighters }));
  }

  onDeleteFighter = (id) => {
    deleteFighter(id).then(() => {
        this.onGetFighters();
    });
  };

  render() {
    const { fighters } = this.state;
    return (
      <div id="figh-wrapper">
        <div>
          <h3>Fighteres List</h3>
          {fighters && fighters.length && (
            <ul className="fighters__list">
              {fighters.map((fighter) => (
                <FighterItem
                  key={fighter.id}
                  fighterData={fighter}
                  deleteFighter={this.onDeleteFighter}
                />
              ))}
            </ul>
          )}
        </div>
      </div>
    );
  }
}
