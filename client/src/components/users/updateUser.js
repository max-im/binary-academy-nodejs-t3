import React, { Component } from 'react';
import { getUser, updateUser } from '../../services/domainRequest/userRequest';
import { withRouter } from 'react-router-dom';

class UpdateUser extends Component {
  state = {
    updated: {},
  };

  componentDidMount() {
    getUser(this.props.match.params.id).then((user) => this.initUser(user));
  }

  initUser = (user) => {
    this.setState({
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      phoneNumber: user.phoneNumber,
    });
  };

  onChange = (e) => {
    const { name, value } = e.target;

    const updated = { ...this.state.updated, [name]: value };
    this.setState({ [name]: value, updated });
  };

  onSubmit = (e) => {
    e.preventDefault();
    updateUser(this.props.match.params.id, this.state.updated).then(() =>
      this.props.history.push('/users')
    );
  };

  render() {
    return (
      <div id="figh-wrapper">
        <div>
          <h3>Update User</h3>
          <form onSubmit={this.onSubmit}>
            <input
              type="text"
              name="firstName"
              value={this.state.firstName}
              onChange={this.onChange}
              placeholder="first name"
            />
            <input
              type="text"
              name="lastName"
              value={this.state.lastName}
              onChange={this.onChange}
              placeholder="last name"
            />
            <input
              type="text"
              name="email"
              value={this.state.email}
              onChange={this.onChange}
              placeholder="email"
            />
            <input
              type="text"
              name="phoneNumber"
              value={this.state.phoneNumber}
              onChange={this.onChange}
              placeholder="phone"
            />
            <button className="mainMenu__link mainMenu__link_active" type="submit">
              Save
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default withRouter(UpdateUser);
