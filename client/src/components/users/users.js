import React, { Component } from 'react';
import { getUsers, deleteUser } from '../../services/domainRequest/userRequest';
import UserItem from './userItem';
import './users.css';

export default class Users extends Component {
  state = {
    users: [],
  };

  componentDidMount() {
      this.onGetUsers();
  }

  onGetUsers() {
    getUsers().then((users) => this.setState({ users }));
  }

  onDeleteUser = (id) => {
    deleteUser(id).then(() => {
        this.onGetUsers();
    });
  };

  render() {
    const { users } = this.state;
    return (
      <div id="figh-wrapper">
        <div>
          <h3>Users List</h3>
          {users && users.length && (
            <ul className="users__list">
              {users.map((user) => (
                <UserItem
                  key={user.id}
                  userData={user}
                  deleteUser={this.onDeleteUser}
                />
              ))}
            </ul>
          )}
        </div>
      </div>
    );
  }
}
