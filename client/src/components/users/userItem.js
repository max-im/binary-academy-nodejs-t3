import React from 'react';
import { Link } from 'react-router-dom';

export default function userItem({ userData, deleteUser }) {
  return (
    <li className="userItem">
      <div>
        <p className="userItem__text">Name: {userData.firstName} {userData.lastName}</p>
        <p className="userItem__text">Email: {userData.email}</p>
        <p className="userItem__text">Phone: {userData.phoneNumber}</p>
        <p className="userItem__text">Created: {userData.createdAt}</p>
        {userData.updatedAt && (
          <p className="userItem__text">Updated: {userData.updatedAt}</p>
        )}
      </div>
      <div className="userItem__control">
        <Link
          className="mainMenu__link mainMenu__link_active"
          to={'/update-user/' + userData.id}
        >
          Update
        </Link>
        <i
          className="fas fa-trash-alt mainMenu__link mainMenu__link_active"
          onClick={() => deleteUser(userData.id)}
        ></i>
      </div>
    </li>
  );
}
