import React from 'react';
import { NavLink } from 'react-router-dom';
import './mainMenu.css';

export default function mainMenu() {
  return (
    <div>
      <nav>
        <ul className="mainMenu">
          <li className="mainMenu__item">
            <NavLink exact to="/" className="mainMenu__link" activeClassName="mainMenu__link_active">Home</NavLink>
          </li>
          <li className="mainMenu__item">
            <NavLink to="/fighters" className="mainMenu__link" activeClassName="mainMenu__link_active">Fighters</NavLink>
          </li>
          <li className="mainMenu__item">
            <NavLink to="/users" className="mainMenu__link" activeClassName="mainMenu__link_active">Users</NavLink>
          </li>
        </ul>
      </nav>
    </div>
  );
}
