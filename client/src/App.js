import React from 'react';
import StartScreen from './components/startScreen';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';

import MainMenu from './components/mainMenu/mainMenu';
import Fighters from './components/fighters/fighters';
import UpdateFighter from './components/fighters/updateFighter';
import Users from './components/users/users';
import UpdateUser from './components/users/updateUser';

function App() {
  return (
    <Router>
      <div>
        <MainMenu />
        <Route exact path="/">
          <MuiThemeProvider>
            <StartScreen />
          </MuiThemeProvider>
        </Route>
        <Route path="/fighters" component={Fighters}></Route>
        <Route path="/users" component={Users}></Route>
        <Route path="/update-user/:id" component={UpdateUser}></Route>
        <Route path="/update-fighter/:id" component={UpdateFighter}></Route>
      </div>
    </Router>
  );
}

export default App;
