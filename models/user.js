exports.user = {
  id: { required: false },
  firstName: { required: true, length: { min: 2, max: 50 } },
  lastName: { required: true, length: { min: 2, max: 50 } },
  email: { required: true, format: /.+@gmail.com/ },
  phoneNumber: { required: true, format: /^\+380\d{9}$/ },
  password: { required: true, length: { min: 3 } },
};
