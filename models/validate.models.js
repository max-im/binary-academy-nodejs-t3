exports.modelValidate = (model, data) => {
  const errors = [];
  const payload = {};

  for(const key in data) {
    if (!(key in model)) {
      errors.push(`${key} field is redundant`);
    }
  }

  if (errors.length) {
    return { error: true, payload: errors };
  }

  for (const key in model) {
    if ('check' in model[key] && model[key].check === false) {
      continue;
    }
  
    // required field is empty
    if (model[key].required && !(key in data && data[key])) {
      errors.push(`${key} field is required`);
      continue;
    }

    let isValidInputField = true;
    // set default value if there is no obtained from user
    if (!data[key] && model[key].default) {
      data[key] = model[key].default;
    }

    // check min length
    if (minLengthError(data, model, key)) {
      errors.push(`${key} length must be at least ${model[key].length.min} characters`);
      isValidInputField = false;
    }

    // check max length
    if (maxLengthError(data, model, key)) {
      errors.push(`${key} length must be less then ${model[key].length.max} characters`);
      isValidInputField = false;
    }

    // check format
    if (formatError(data, model, key)) {
      errors.push(`${key} field is invalid`);
      isValidInputField = false;
    }

    // check numbers input
    if (shouldCheckNumber(data, model, key)) {
      const numberError = checkInputNumbers(data, model, key);
      if (numberError) {
        errors.push(numberError);
        isValidInputField = false;
      }
    }

    if (isValidInputField) {
      payload[key] = data[key];
    }
  }
  return { error: errors.length > 0, payload: errors.length > 0 ? errors : payload };
};

exports.validateField = (data, model, key) => {
  const errors = []
  if (model[key].required && !data[key].length) {
    errors.push(`${key} field is required`);
    return;
  };
  if (minLengthError(data, model, key)) {
    errors.push(`${key} length must be at least ${model[key].length.min} characters`);
  }
  if (maxLengthError(data, model, key)) {
    errors.push(`${key} length must be less then ${model[key].length.max} characters`);
  }
  if (formatError(data, model, key)) {
    errors.push(`${key} field is invalid`);
  }
  if (shouldCheckNumber(data, model, key)) {
    const numberError = checkInputNumbers(data, model, key);
    if (numberError) {
      errors.push(numberError);
    }
  }
  return errors;
}

function minLengthError(data, model, key) {
  return (
    'length' in model[key] && 'min' in model[key].length && data[key].length < model[key].length.min
  );
}

function maxLengthError(data, model, key) {
  return (
    'length' in model[key] && 'max' in model[key].length && data[key].length > model[key].length.max
  );
}

function formatError(data, model, key) {
  return 'format' in model[key] && !data[key].match(model[key].format);
}

function shouldCheckNumber(data, model, key) {
  return 'type' in model[key] && model[key].type === 'number';
}

function checkInputNumbers(data, model, key) {
  const numValue = parseInt(data[key]);
  let error = null;
  if (isNaN(numValue)) {
    error = `${key} value must be a number`;
  } else if (model[key].numbers.min && numValue < model[key].numbers.min) {
    error = `${key} value must be more then ${model[key].numbers.min}`;
  } else if (model[key].numbers.max && numValue > model[key].numbers.max) {
    error = `${key} value must be less then ${model[key].numbers.max}`;
  }
  return error;
}
