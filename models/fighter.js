exports.fighter = {
  id: { check: false },
  name: { required: true, length: { min: 3, max: 50 } },
  health: { required: false, default: 100 , type: 'number', numbers: {min: 10, max: 100}},
  power: { required: false, type: 'number', numbers: {min: 1, max: 100 } },
  defense: { required: false, type: 'number', numbers: {min: 1, max: 100 } },
};
