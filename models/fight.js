exports.fight = {
    "id": { check: false },
    "fighter1": {required: true},
    "fighter2": {required: true},
    "log": { check: false }
    // [
        // {
        //     "fighter1Shot": 0,
        //     "fighter2Shot": 0,
        //     "fighter1Health": 0,
        //     "fighter2Health": 0
        // }
    // ]
}